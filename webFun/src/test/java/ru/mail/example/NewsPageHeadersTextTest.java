package ru.mail.example;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import ru.mail.example.pages.MailMainPage;
import ru.mail.example.pages.MailNewsPage;

import java.util.List;
import java.util.concurrent.TimeUnit;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class NewsPageHeadersTextTest {

    private WebDriver driver;

    @BeforeAll
    void setDriver() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().setSize(new Dimension(414,2222));
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    }

    @Test
    void getHeadersTExtTest() {
        MailMainPage mailMainPage = new MailMainPage(driver);
        mailMainPage.getMailPage();
        MailNewsPage newsPage = mailMainPage.getNewsPage();
        List<String> headerNews = newsPage.getHeaderNews();
        Assertions.assertEquals(headerNews.size(),8);
        Assertions.assertTrue(headerNews.contains("Общество"));
    }

    @AfterAll
    void quit(){
        driver.quit();
    }
}
