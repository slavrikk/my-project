package ru.mail.example.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MailMainPage {

    @FindBy(xpath = "//button[@data-testid='enter-mail-primary']")
    private WebElement signInButton;

    @FindBy(xpath = "//a[@data-click-counter='360866032, 61021860']")
    private WebElement newsElement;

    public final static String BASE_MAIN_PAGE_URL = "https://mail.ru/";

    private final WebDriver driver;

    public MailMainPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void signIn() {
        signInButton.click();
    }

    public void getMailPage() {
        driver.get(BASE_MAIN_PAGE_URL);
    }

    public MailNewsPage getNewsPage() {
        newsElement.click();
        //switch window
        String originalWindow = driver.getWindowHandle();
        for (String windowHandle : driver.getWindowHandles()) {
            if (!originalWindow.contentEquals(windowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }
        return new MailNewsPage(driver);
    }


}
