package ru.mail.example.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.stream.Collectors;

public class MailNewsPage {

    private final WebDriver driver;

    @FindBy(xpath = "//span[@class='js-text-inner pm-toolbar__button__text__inner  pm-toolbar__button__text__inner_submenu pm-toolbar__button__text__inner_noicon pm-toolbar__button__text__inner_submenu']")
    private List<WebElement> headersElements;

    public MailNewsPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public List<String> getHeaderNews() {
        return headersElements.stream()
                .map(WebElement::getText)
                .peek(System.out::println)
                .collect(Collectors.toList());
    }

}
