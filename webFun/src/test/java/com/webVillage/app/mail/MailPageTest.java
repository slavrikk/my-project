package com.webVillage.app.mail;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class MailPageTest {

    private WebDriver driver;

    @BeforeAll
    void setDriver() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    @Test
    void getGoogleTest() {
        driver.get("https://mail.ru/");
        WebElement enterButton = driver.findElement(By.xpath("//button[@data-testid='enter-mail-primary']"));
        enterButton.click();

        //get frame element
        WebElement frame = driver.findElement(By.xpath("//iframe[@class='ag-popup__frame__layout__iframe']"));

        //set to switch
        driver.switchTo().frame(frame);

        //elements in iframe
        WebElement driverElement = driver.findElement(By.xpath("//div[@id='login-content']//input[@name='username']"));
        driverElement.sendKeys("email_from_frame");
        WebElement button = driver.findElement(By.xpath("//button[@data-test-id='next-button']"));
        button.click();

    }

    @AfterAll
    public void afterTest() {
        // driver.quit();
    }

}
