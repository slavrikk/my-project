package com.webVillage.app.remote;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

//https://github.com/SeleniumHQ/docker-selenium
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SeleniumJenkinsRemoteGoogleExample {

    public static String remote_url_chrome = "http://172.17.0.3:4444/wd/hub";

    private WebDriver driver;

    @BeforeAll
    void setDriver() throws MalformedURLException {
        ChromeOptions options = new ChromeOptions();
        driver = new RemoteWebDriver(new URL(remote_url_chrome), options);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    @Test
    void getGoogleTest() {
        driver.get("https://www.google.com/");
        WebElement element = driver.findElement(By.xpath("//input[@title='Поиск']"));
        element.sendKeys("selenium");
        element.sendKeys(Keys.ENTER);
    }

    @AfterAll
    public void afterTest() {
        driver.quit();
    }

}
