package com.webVillage.app.webControllers;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SeleniumGoogleExample {

    private WebDriver driver;

    @BeforeAll
    void setDriver() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    @Test
    void getGoogleTest() {
        String remote = System.getProperty("remote_driver");
        driver.get("https://www.google.com/");
        WebElement element = driver.findElement(By.xpath("//input[@title='Поиск']"));
        element.sendKeys("selenium");
        element.sendKeys(Keys.ENTER);
    }

    /*
    @AfterAll
    public void afterTest() {
        driver.quit();
    }

     */

}
