create table request
(
    id        integer not null,
    user_sum  integer,
    user_name varchar(100),
    user_age  integer,
    constraint pk_request_id primary key (id)
);
