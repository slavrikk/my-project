CREATE OR REPLACE FUNCTION insert_data_request(in id_ integer, in user_age integer,in user_name varchar,in user_sum integer) returns void
LANGUAGE plpgsql
BEGIN
    INSERT INTO request(id,user_age,user_name,user_sum) VALUES (id_, user_age, user_name, user_sum);
END;
