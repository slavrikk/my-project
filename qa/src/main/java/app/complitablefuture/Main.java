package app.complitablefuture;

import app.complitablefuture.dto.FullInfo;
import app.complitablefuture.dto.LoginDto;
import app.complitablefuture.dto.ReposFullNameDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public class Main {

    public static void main(String[] args) {

        long start = System.currentTimeMillis();
        HttpClient httpClient = HttpClient.newHttpClient();
        HttpRequest httpRequestLogin = HttpRequest.newBuilder()
                .uri(URI.create("https://api.github.com/users/slavrikk"))
                .GET()
                .build();

        HttpRequest httpRequestRepos = HttpRequest.newBuilder()
                .uri(URI.create("https://api.github.com/users/slavrikk/repos"))
                .GET()
                .build();

        CompletableFuture<HttpResponse<String>> getLogin = httpClient.sendAsync(httpRequestLogin, HttpResponse.BodyHandlers.ofString());

        CompletableFuture<LoginDto> loginDto = getLogin.thenApply((response) -> {
            try {
                return parseLogin(response.body());
            } catch (JsonProcessingException e) {
                e.printStackTrace();
                return null;
            }
        });

        CompletableFuture<Void> getRepos = httpClient.sendAsync(httpRequestRepos, HttpResponse.BodyHandlers.ofString())
                .thenApply(response -> {
                    try {
                        return parseReposAsList(response.body());
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                        return null;
                    }
                })
                .thenCombine(loginDto, (repos, login) -> {
                    FullInfo fullInfo = new FullInfo();
                    fullInfo.setLoginDto(login);
                    fullInfo.setReposFullNameDtos(repos);
                    return fullInfo;
                })//.exceptionally()
                .thenAccept(fullInfo -> {
                    System.out.println("Login: " + fullInfo.getLoginDto().getLogin());
                    fullInfo.getReposFullNameDtos().forEach(repo -> System.out.println("Repo: " + repo.getFullName()));
                });

        getRepos.join();
        System.out.println(System.currentTimeMillis() - start);
    }

    private static LoginDto parseLogin(String json) throws JsonProcessingException {
        Gson gson = new Gson();
        return gson.fromJson(json, LoginDto.class);
    }

    private static List<ReposFullNameDto> parseReposAsList(String json) throws JsonProcessingException {
        Gson gson = new Gson();
        return gson.fromJson(json, new TypeToken<List<ReposFullNameDto>>() {
        }.getType());
    }
}
