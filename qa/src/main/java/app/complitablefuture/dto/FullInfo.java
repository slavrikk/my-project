package app.complitablefuture.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FullInfo {

    private LoginDto loginDto;

    private List<ReposFullNameDto> reposFullNameDtos;
}
