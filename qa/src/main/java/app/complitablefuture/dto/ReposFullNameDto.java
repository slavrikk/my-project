package app.complitablefuture.dto;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class ReposFullNameDto {

    @SerializedName("full_name")
    private String fullName;
}
