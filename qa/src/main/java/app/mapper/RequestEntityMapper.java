package app.mapper;

import app.entity.RequestEntity;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RequestEntityMapper implements RowMapper<RequestEntity> {

    @Override
    public RequestEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
        RequestEntity requestEntity = new RequestEntity();
        requestEntity.setUserAge(rs.getInt("user_age"));
        requestEntity.setUserName(rs.getString("user_name"));
        requestEntity.setUserSum(rs.getLong("user_sum"));
        requestEntity.setId(rs.getInt("id"));
        return requestEntity;
    }
}
