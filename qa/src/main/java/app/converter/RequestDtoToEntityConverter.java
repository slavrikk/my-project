package app.converter;

import app.dto.credit.RequestCredit;
import app.dto.credit.UserForCredit;
import app.entity.RequestEntity;
import org.springframework.stereotype.Service;

@Service
public class RequestDtoToEntityConverter {

    public RequestEntity convertUser(UserForCredit user) {
        RequestEntity requestEntity = new RequestEntity();
        requestEntity.setUserAge(user.getAge());
        requestEntity.setUserName(user.getName());
        requestEntity.setUserSum((long) user.getSum());
        return requestEntity;
    }

    public RequestCredit convertRequestEntity(RequestEntity requestEntity) {
        RequestCredit requestCredit = new RequestCredit();
        UserForCredit user = new UserForCredit();
        user.setAge(requestEntity.getUserAge());
        user.setSum(Math.toIntExact(requestEntity.getUserSum()));
        user.setName(requestEntity.getUserName());
        requestCredit.setId(requestEntity.getId());
        requestCredit.setUser(user);
        return requestCredit;
    }
}
