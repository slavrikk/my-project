package app.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "request")
@Data
@AllArgsConstructor
@NoArgsConstructor
@NamedStoredProcedureQuery(name = "RequestEntity.saveEntity",
        procedureName = "insert_data_request", parameters = {
        @StoredProcedureParameter(mode = ParameterMode.IN, name = "id_", type = Integer.class),
        @StoredProcedureParameter(mode = ParameterMode.IN, name = "user_age", type = Integer.class),
        @StoredProcedureParameter(mode = ParameterMode.IN, name = "user_name", type = String.class),
        @StoredProcedureParameter(mode = ParameterMode.IN, name = "user_sum", type = Integer.class)})
public class RequestEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "user_sum")
    private Long userSum;

    @Column(name = "user_name",length = 100)
    private String userName;

    @Column(name = "user_age")
    private Integer userAge;
}
