package app.service;

import app.converter.RequestDtoToEntityConverter;
import app.dto.credit.RequestCredit;
import app.dto.credit.UserForCredit;
import app.entity.RequestEntity;
import app.mapper.RequestEntityMapper;
import app.repository.RequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class RequestService {

    @Autowired
    private RequestRepository requestRepository;

    @Autowired
    private RequestDtoToEntityConverter converter;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static final String SQL_QUERY_FOR_LESS_SUM = "SELECT * FROM REQUEST WHERE USER_SUM = ?";

    @Transactional
    public int saveEntity(UserForCredit user) {
        RequestEntity requestEntity = converter.convertUser(user);
        RequestEntity entity = requestRepository.save(requestEntity);
        return Math.toIntExact(entity.getId());
    }

    @Transactional(readOnly = true)
    public RequestCredit getUserRequestById(int id) {
        RequestEntity requestEntity = requestRepository.getById(id);
        return converter.convertRequestEntity(requestEntity);
    }

    @Transactional(readOnly = true)
    public List<RequestCredit> getUserRequests() {
        List<RequestEntity> requestEntities = requestRepository.findAll();

        return requestEntities.stream()  //Stream API () -> { }
                //RequestEntity1 RequestEntity2     List<RequestEntity>  ->    List<RequestCredit>
                //^                                 //RequestCredit
                .map(requestEntity -> converter.convertRequestEntity(requestEntity))
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public RequestCredit getUserWithSum(int sum) {
        RequestEntity requestEntity = jdbcTemplate.queryForObject(SQL_QUERY_FOR_LESS_SUM, new RequestEntityMapper(), sum);
        return converter.convertRequestEntity(Objects.requireNonNull(requestEntity));
    }

    @Transactional
    public void update(int id, UserForCredit user) {
        RequestEntity entity = requestRepository.findById(id).orElseThrow();
        entity.setUserAge(user.getAge());
        //requestRepository.save(entity);
    }
}
