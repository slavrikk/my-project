package app.service;

import app.dto.credit.UserForCredit;
import org.springframework.stereotype.Service;

@Service
public class CreditCalculatorService {

    private final String NOT_EQUALS_NAME = "Bob";

    public boolean calculateCredit(UserForCredit person) {
        boolean creditResult = false;
        if (person.getAge() > 18 && !person.getName().equals(NOT_EQUALS_NAME)) {
            creditResult = true;
        }
        return creditResult;

    }
}
