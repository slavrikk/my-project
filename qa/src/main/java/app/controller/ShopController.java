package app.controller;

import app.entity.example.shop.Suppliers;
import app.repository.SuppliersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/shop")
public class ShopController {

    @Autowired
    private SuppliersRepository suppliersRepository;

    @GetMapping("/all")
    public List<Suppliers> getSuppliers(){
        return suppliersRepository.findAll();
    }
}
