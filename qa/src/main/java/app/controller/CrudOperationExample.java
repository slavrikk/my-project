package app.controller;

import app.dto.User;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class CrudOperationExample {

    Map<Integer, User> map = new HashMap<>();

    @GetMapping("/{id}")
    public User get(@PathVariable int id) {
        return map.get(id);
    }

    @GetMapping("/all")
    public List<User> get() {
        return new ArrayList<>(map.values());
    }

    @PostMapping()
    public void save(@RequestBody @Valid User user) {
        int id = map.size() + 1;
        user.setId(id);
        map.put(id, user);
    }

    @PutMapping("/{id}")
    public void change(@PathVariable int id, @RequestBody User userChanging) {
        User user = map.get(id);
        user.setAge(userChanging.getAge());
        user.setName(userChanging.getName());
        map.put(id, user);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        map.remove(id);
    }
}
