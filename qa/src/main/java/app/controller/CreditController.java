package app.controller;

import app.dto.credit.RequestCredit;
import app.dto.credit.UserForCredit;
import app.service.CreditCalculatorService;
import app.service.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/credit") //   http://localhost:8080/credit
public class CreditController {

    @Autowired
    private CreditCalculatorService creditCalculatorService;

    @Autowired
    private RequestService requestService;

    //List<RequestCredit> requestCreditList = new ArrayList<>();

    @PostMapping("/request")  // http://localhost:8080/credit/request
    // body:  json {....}
    public Integer creditResult(@RequestBody UserForCredit user) {
        /*
        int id = requestCreditList.size() + 1;
        RequestCredit requestCredit = new RequestCredit();
        requestCredit.setId(id);
        requestCredit.setUser(user);
        requestCreditList.add(requestCredit);

         */
        return requestService.saveEntity(user);
    }

    @GetMapping("/{id}")  //http://localhost:8080/credit/24
    public RequestCredit get(@PathVariable int id) {
        /*
        return requestCreditList.stream()
                .filter(requestCredit -> requestCredit.getId() == id)
                .findFirst()
                .orElseThrow();
         */
        return requestService.getUserRequestById(id);
    }


    @GetMapping("/all")  //http://localhost:8080/credit/all
    public List<RequestCredit> get() {
        /*
        return requestCreditList;
         */
        return requestService.getUserRequests();
    }

    @GetMapping("/sum/{sum}")
    public RequestCredit requestSum(@PathVariable int sum, @RequestParam String aad) {
        /*
        return requestCreditList;
         */
        return requestService.getUserWithSum(sum);
    }

    @PostMapping("/result")
    public Boolean creditResult(@RequestBody RequestCredit requestCredit) {
        //requestCreditList.add(requestCredit);
        return creditCalculatorService.calculateCredit(requestCredit.getUser());
    }

    @PutMapping("/{id}")
    public void change(@PathVariable int id, @RequestBody UserForCredit user) {
        requestService.update(id,user);
    }

    //todo implement delete requests
    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {

    }

}
