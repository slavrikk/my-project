package app.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.TreeMap;

@RestController
public class MultiController {

    @GetMapping("/multi/{number}")
    public Map<Integer, Long> test(@PathVariable int number) {
        Map<Integer, Long> map = new TreeMap<>();
        for (int i = 1; i <= number; i++) {
            map.put(i, (long) Math.pow(number, i));
        }
        return map;
    }
}
