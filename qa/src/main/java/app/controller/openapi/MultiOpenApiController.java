package app.controller.openapi;

import app.api.MultiApi;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

@RestController
public class MultiOpenApiController implements MultiApi {

    @Override
    public ResponseEntity<Map<String, Integer>> multiplication(Integer number, String param) {
        Map<String, Integer> map = new TreeMap<>();
        map.put(Objects.requireNonNullElse(param, "Null param"), 0);
        for (int i = 1; i <= number; i++) {
            map.put(String.valueOf(i), (int) Math.pow(number, i));
        }
        return ResponseEntity.ok(map);
    }
}
