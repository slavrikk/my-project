package app.controller.openapi;

import app.api.HelloApi;
import app.models.Hello;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloOpenApiController implements HelloApi {

    @Override
    public ResponseEntity<Hello> hello() {
        Hello hello = new Hello();
        hello.text("Hello");
        return ResponseEntity.ok(hello);
    }
}
