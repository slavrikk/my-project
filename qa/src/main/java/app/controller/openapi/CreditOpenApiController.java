package app.controller.openapi;

import app.api.CreditApi;
import app.controller.handler.exception.BobSaveToDbException;
import app.dto.credit.RequestCredit;
import app.dto.credit.UserForCredit;
import app.models.RequestCreditOApi;
import app.models.UserCredit;
import app.repository.RequestRepository;
import app.service.RequestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@Slf4j
public class CreditOpenApiController implements CreditApi {

    @Autowired
    private RequestService requestService;

    @Autowired
    private RequestRepository requestRepository;

    @Override
    public ResponseEntity<List<RequestCreditOApi>> getRequests() {
        List<RequestCredit> userRequests = requestService.getUserRequests();
        return ResponseEntity.ok(userRequests.stream()
                .map(userRequest -> {
                    RequestCreditOApi requestCreditOApi = new RequestCreditOApi();
                    requestCreditOApi.age(userRequest.getUser().getAge())
                            .sum(userRequest.getUser().getSum())
                            .name(userRequest.getUser().getName())
                            .id(userRequest.getId());
                    return requestCreditOApi;
                })
                .collect(Collectors.toList()));
    }

    @Override
    public ResponseEntity<String> saveRequest(@Valid @RequestBody UserCredit userCredit) {

        UserForCredit user = new UserForCredit(userCredit.getSum(),
                userCredit.getName(), userCredit.getAge());
        if (userCredit.getName().equals("Bob")) {
            throw new BobSaveToDbException("Bob does not have permission to get a credit!");
        }
        return ResponseEntity.ok(String.valueOf(requestService.saveEntity(user)));

    }

    @Override
    public ResponseEntity<String> saveRequestPsql(@Valid RequestCreditOApi userCredit) {
        try {
            requestRepository.insertRequestData(userCredit.getId(),
                    userCredit.getAge(), userCredit.getName(), userCredit.getSum());
        } catch (Exception e) {
            log.warn(e.getMessage());
        }
        return ResponseEntity.ok("OK");
    }
}
