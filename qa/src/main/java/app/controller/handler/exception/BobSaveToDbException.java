package app.controller.handler.exception;

public class BobSaveToDbException extends RuntimeException {
    public BobSaveToDbException(String message) {
        super(message);
    }
}
