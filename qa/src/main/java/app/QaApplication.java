package app;

import app.entity.example.shop.Goods;
import app.entity.example.shop.Manufacturer;
import app.entity.example.shop.Suppliers;
import app.repository.ManufacturerRepository;
import app.repository.SuppliersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.Transactional;
import springfox.documentation.spring.data.rest.configuration.SpringDataRestConfiguration;

import java.util.HashSet;

@SpringBootApplication
public class QaApplication {

    @Autowired
    private SuppliersRepository suppliersRepository;

    @Autowired
    private ManufacturerRepository manufacturerRepository;

    public static void main(String[] args) {
        SpringApplication.run(QaApplication.class, args);
    }


    @Bean
    public CommandLineRunner commandLineRunner() {
        return (args) -> {
            add();
        };
    }

    @Transactional
    void add() {
        //save manufacturer
        Manufacturer manufacturer = new Manufacturer();
        manufacturer.setInn(123455);
        manufacturer.setName("Manufacturer 1");
        Manufacturer save = manufacturerRepository.save(manufacturer);

        //save supplier
        Suppliers suppliers = new Suppliers();
        suppliers.setName("Supplier 1");
        suppliers.setManufacturer(manufacturer);

        //save goods
        Goods goods = new Goods();
        goods.setName("Goods 1");
        goods.setSuppliers(suppliers);

        HashSet<Goods> goodsSet = new HashSet<>();
        goodsSet.add(goods);
        suppliers.setGoods(goodsSet);
        suppliersRepository.save(suppliers);
    }
}
