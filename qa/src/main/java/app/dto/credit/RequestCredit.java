package app.dto.credit;

import lombok.Data;

@Data
public class RequestCredit {
    private int id;
    private UserForCredit user;
}
