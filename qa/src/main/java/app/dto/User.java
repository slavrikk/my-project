package app.dto;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@Data
public class User {

    private int id;
    @NotEmpty(message = "The name is required.")
    private String name;
    @Min(value = 18)
    private int age;
}
