package app.repository;

import app.entity.RequestEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;

public interface RequestRepository extends JpaRepository<RequestEntity, Integer> {

    @Procedure(name = "RequestEntity.saveEntity")
    void insertRequestData(@Param("id_") Integer id, @Param("user_age") Integer userAge, @Param("user_name") String userName
            , @Param("user_sum") Integer userSum);
}
