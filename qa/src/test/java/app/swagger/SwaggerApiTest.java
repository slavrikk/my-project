package app.swagger;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.DirtiesContext;

import java.util.LinkedHashMap;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SwaggerApiTest {

    @LocalServerPort
    public int port;

    public RequestSpecification request;

    @BeforeEach
    public void createRestRequest() {
        request = RestAssured.given();
        RestAssured.baseURI = "http://localhost:" + port;
    }

    @Test
    void swaggerApiMethodsValidationTest() {
        JsonPath body = request
                .baseUri(RestAssured.baseURI)
                .log().all()
                .when()
                .get("/v2/api-docs")
                .then().log().all()
                .statusCode(HttpStatus.OK.value())
                .extract()
                .body()
                .jsonPath();

        LinkedHashMap paths = body.getJsonObject("paths");
        //Assertions.assertEquals(paths.size(), 9);
        Assertions.assertTrue(paths.containsKey("/credit/all"));
        Assertions.assertTrue(paths.containsKey("/credit/request"));
        Assertions.assertTrue(paths.containsKey("/credit/result"));
        Assertions.assertTrue(paths.containsKey("/credit/{id}"));
        Assertions.assertTrue(paths.containsKey("/user"));
        Assertions.assertTrue(paths.containsKey("/user/all"));
        Assertions.assertTrue(paths.containsKey("/user/{id}"));
    }
}
