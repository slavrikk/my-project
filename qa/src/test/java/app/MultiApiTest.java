package app;

import app.dto.credit.RequestCredit;
import app.dto.credit.UserForCredit;
import app.entity.RequestEntity;
import app.repository.RequestRepository;
import io.qameta.allure.Description;
import io.restassured.RestAssured;
import io.restassured.common.mapper.TypeRef;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.DirtiesContext;

import java.net.http.HttpClient;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

//https://github.com/rest-assured/rest-assured/wiki/Usage
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MultiApiTest {

    @LocalServerPort
    public int port;

    public RequestSpecification request;

    @Autowired
    private RequestRepository requestRepository;

    //@BeforeAll //Optional
    void setData() {
        RequestEntity requestEntity = new RequestEntity();
        requestEntity.setUserSum(2000L);
        requestEntity.setUserName("Name From data");
        requestEntity.setUserAge(50);
        requestRepository.save(requestEntity);
    }


    @AfterEach
    void delete() {
        requestRepository.deleteAll();
    }

    @BeforeEach
    public void createRestRequest() {
        request = RestAssured.given();
        Random random =new Random();
        RandomString randomString = new RandomString();
        randomString.nextString();

        String sasd = "asd";
        ArrayList<String> arrayList = new ArrayList<>();
        List<String> collect = arrayList.stream()
                .flatMap(s -> Stream.of(s.toLowerCase()))
                .flatMap(str -> Stream.of(str.substring(0, 1).toUpperCase() + str.substring(1)))
                .collect(Collectors.toList());
        RestAssured.baseURI = "http://localhost:" + port;
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7})
    @Description("TEST")
    void multiControllerTest(int number) {
        Map<String, Integer> response = request
                .baseUri(RestAssured.baseURI)
                .log().all()
                .when()
                .get("/multi/" + number)
                .then()
                .log().all()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.OK.value())
                .extract()
                .body()
                .as(new TypeRef<Map<String, Integer>>() {
                });

        for (String key : response.keySet()) {
            Assertions.assertEquals(response.get(key), (int) Math.pow(number, Double.parseDouble(key)));
        }
    }

    @Test
    //Получение заявок, сохраненных до этого в базе
    void getRequestsWhichHaveBeenSavedByRepositoryTest() {
        //сохранение сущности
        //pre-conditions
        setData();

        //запрос
        //step 1
        List<RequestCredit> requestCreditList = Arrays.asList(request
                .baseUri(RestAssured.baseURI)
                .log().all()
                .when()
                .get("/credit/all")
                .then().log().all()
                .statusCode(HttpStatus.OK.value())
                .extract()
                .as(RequestCredit[].class));

        //assert
        Assertions.assertEquals(requestCreditList.size(), 1);

        UserForCredit userForCredit = requestCreditList.stream()
                .map(RequestCredit::getUser)
                .filter(requestCreditUser -> requestCreditUser.getName().equals("Name From data"))
                .findFirst()
                .orElseThrow();

        Assertions.assertEquals(userForCredit.getName(), "Name From data");
        Assertions.assertEquals(userForCredit.getAge(), 50);
        Assertions.assertEquals(userForCredit.getSum(), 2000);

        ///
    }

    @Test
    void getRequestsTest() {
        //pre-conditions
        UserForCredit user = new UserForCredit(1000, "Name", 18);

        Integer response = request
                .baseUri(RestAssured.baseURI)
                .log().all()
                .contentType(ContentType.JSON)
                .when()
                .body(user)
                .post("/credit/request")
                .then().log().all()
                .statusCode(HttpStatus.OK.value())
                .extract()
                .as(Integer.class);

        Assertions.assertNotNull(response);

        ///
        List<RequestCredit> requestCreditList = Arrays.asList(request
                .baseUri(RestAssured.baseURI)
                .log().all()
                .when()
                .get("/credit/all")
                .then().log().all()
                .statusCode(HttpStatus.OK.value())
                .extract()
                .as(RequestCredit[].class));

        Assertions.assertEquals(requestCreditList.size(), 1);

        UserForCredit userForCredit = requestCreditList.stream()
                .map(RequestCredit::getUser)
                .filter(requestCreditUser -> requestCreditUser.getName().equals("Name"))
                .findFirst()
                .orElseThrow();

        Assertions.assertEquals(userForCredit.getName(), "Name");
        Assertions.assertEquals(userForCredit.getAge(), 18);
        Assertions.assertEquals(userForCredit.getSum(), 1000);

        ///
    }


}
