package village;

public enum Race {

    Peasant,
    Witch,
    Vampire,
    Werewolf

}
