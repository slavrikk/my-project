package village;

import com.github.javafaker.Faker;
import service.save.EventMessageToFileSaver;
import village.race.Peasant;
import village.race.Vampire;
import village.race.Werewolf;
import village.race.Witch;

import java.io.IOException;
import java.util.*;

public class Village {

    private final static int CITIZENS_QUANTITY = 50;
    private final static Random random = new Random();
    private static final int FINAL_YEAR = 1651;
    private static final Calendar startLife = new GregorianCalendar(1650, Calendar.JANUARY, 1);
    private static final Calendar endLife = new GregorianCalendar(FINAL_YEAR, Calendar.JANUARY, 1);
    private static final List<Calendar> moonLightDays = new ArrayList<>();

    private static final List<Citizen> citizens = new ArrayList<>();
    private static final boolean saveToFile = true;
    private static EventMessageToFileSaver eventMessageToFileSaver;


    public static void main(String[] args) throws IOException {

        generateCitizens();
        getMoonLightDays();
        if (saveToFile) {
            eventMessageToFileSaver = new EventMessageToFileSaver();
            eventMessageToFileSaver.init();
        }
        lifeCycle();


    }

    public static void generateCitizens() {

        for (int i = 0; i < CITIZENS_QUANTITY; i++) {
            int citizenRace = random.nextInt(4); // 0 1 2 3
            citizens.add(getRace(citizenRace));
        }
    }

    public static void lifeCycle() {
        while (startLife.before(endLife)) {

            int action = random.nextInt(4);
            System.out.println("citizens: " + citizens.size());
            System.out.println("action " + action);
            System.out.println("Day : " + startLife.get(Calendar.DAY_OF_MONTH) + "." + startLife.get(Calendar.MONTH) + "." + startLife.get(Calendar.YEAR) + " " + startLife.get(Calendar.DAY_OF_WEEK));
            switch (action) {
                case 1: {
                    int citizenRace = random.nextInt(4);
                    Citizen citizen = getRace(citizenRace);
                    citizens.add(citizen);
                    System.out.println(citizen + " come to village");
                    saveToFile(citizen + " come to village");

                    break;
                }
                case 0: {
                    if (!citizens.isEmpty()) {
                        int citizen = random.nextInt(citizens.size());
                        Citizen citizenRemove = citizens.remove(citizen);
                        System.out.println(citizenRemove + " go out from village");
                        saveToFile(citizenRemove + " go out from village");
                    }
                    break;

                }
                case 3:
                    if (!citizens.isEmpty()) {
                        int citizenRandomOne = random.nextInt(citizens.size());
                        int citizenRandomTwo = random.nextInt(citizens.size());
                        goVisiting(citizens.get(citizenRandomOne), citizens.get(citizenRandomTwo));
                    }
                    break;
                default:
                    System.out.println("Sleeping....");
                    saveToFile("Sleeping...");
                    break;
            }
            startLife.add(Calendar.DATE, 1);
        }
    }

    public static Citizen getRace(int race) {
        Citizen citizen = null;
        Faker faker = new Faker();
        switch (race) {
            case 0:
                citizen = new Peasant(faker.name().firstName(), faker.name().lastName());
                break;
            case 1:
                citizen = new Vampire(faker.name().firstName(), faker.name().lastName());
                break;
            case 2:
                citizen = new Werewolf(faker.name().firstName(), faker.name().lastName());
                break;
            case 3:
                citizen = new Witch(faker.name().firstName(), faker.name().lastName());
                break;
        }

        return citizen;
    }

    private static void goVisiting(Citizen citizenOne, Citizen citizenTwo) {
        //condition 1
        if (!citizenOne.getRace().equals(Race.Witch) &&
                ((Village.startLife.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY && Village.startLife.get(Calendar.DAY_OF_MONTH) == 13) ||
                        (Village.startLife.get(Calendar.DAY_OF_MONTH) == 31 && Village.startLife.get(Calendar.MONTH) == Calendar.OCTOBER))) {

            Witch witch = (Witch) citizens //5 - witch (1)    ///       10- any
                    .stream() //Объект Stream (гуглить Stream API)
                    .filter(citizen -> citizen.getRace().equals(Race.Witch)) //фильтер
                    .findFirst()
                    .orElse(null);
            if (witch != null) {
                System.out.println("MURDER_EXCEPTION " + citizenOne + " " + "has been killed by " + witch);
                saveToFile("MURDER_EXCEPTION " + citizenOne + " " + "has been killed by " + witch);
                citizens.remove(citizenOne);
            }
            System.out.println("Is nothing happened in visiting");
            return;
        }
        //condition 2
        if (citizenOne.getRace().equals(Race.Vampire) || citizenTwo.getRace().equals(Race.Vampire)) {
            Citizen checkVampire = (citizenOne.getRace().equals(Race.Vampire) ? citizenOne : citizenTwo);
            //condition 2.3
            if (1 >= random.nextInt(100)) { //1..100
                System.out.println("DawnException " + checkVampire + " get out from the village");
                saveToFile("DawnException " + checkVampire + " get out from the village");
                citizens.remove(checkVampire);
                return;
            }
            //condition 2.2
            if (4 >= random.nextInt(100)) {
                if (citizenOne.getRace().equals(Race.Vampire)) {
                    System.out.println("MURDER_EXCEPTION " + citizenTwo + " has been a Vampire by " + citizenOne);
                    saveToFile("MURDER_EXCEPTION " + citizenTwo + " has been a Vampire by " + citizenOne);
                    citizens.remove(citizenTwo);
                } else {
                    System.out.println("MURDER_EXCEPTION " + citizenOne + " has been a Vampire by " + citizenTwo);
                    saveToFile("MURDER_EXCEPTION " + citizenOne + " has been a Vampire by " + citizenTwo);
                    citizens.remove(citizenOne);
                }
                return;
            }
            //condition 2.1
            if ((citizenOne.getRace().equals(Race.Peasant) || citizenOne.getRace().equals(Race.Witch)) || (
                    citizenTwo.getRace().equals(Race.Peasant) || citizenTwo.getRace().equals(Race.Witch)
            )) {
                if (5 >= random.nextInt(100)) {
                    if (citizenOne.getRace().equals(Race.Peasant) || citizenOne.getRace().equals(Race.Witch)) {
                        System.out.println("Citizen " + citizenOne + " is a Vampire");
                        saveToFile("Citizen " + citizenOne + " is a Vampire");
                        citizenOne.setRace(Race.Vampire);
                        citizens.add(citizenOne);
                        return;
                    }
                    if (citizenTwo.getRace().equals(Race.Peasant) || citizenTwo.getRace().equals(Race.Witch)) {
                        System.out.println("Citizen " + citizenTwo + " is a Vampire");
                        saveToFile("Citizen " + citizenTwo + " is a Vampire");
                        citizenTwo.setRace(Race.Vampire);
                        citizens.add(citizenTwo);
                        return;
                    }
                }
            }
            return;
        }

        //condition 3
        if (moonLightDays.contains(startLife)) {
            Werewolf werewolf = (Werewolf) citizens.stream()
                    .filter(c -> c.getRace().equals(Race.Werewolf))
                    .findFirst()
                    .orElse(null);

            if (werewolf != null) {
                System.out.println("MURDER_EXCEPTION " + citizenOne + " has been killed by " + werewolf);
                saveToFile("MURDER_EXCEPTION " + citizenOne + " has been killed by " + werewolf);
                citizens.remove(citizenOne);
            }
            return;
        }

        //condition 4
        System.out.println(citizenOne + " drinks tea with " + citizenTwo);
        saveToFile(citizenOne + " drinks tea with " + citizenTwo);
    }

    public static void getMoonLightDays() {
        //start moonlight day
        GregorianCalendar firstMoonLightDay = new GregorianCalendar(1650, Calendar.JANUARY, 1);
        while (firstMoonLightDay.before(endLife)) {
            firstMoonLightDay.add(Calendar.DAY_OF_MONTH, 28);
            if (firstMoonLightDay.before(endLife)) {
                GregorianCalendar newCal = new GregorianCalendar();
                newCal.setTime(firstMoonLightDay.getTime());
                moonLightDays.add(newCal);
            }
        }
    }

    private static void saveToFile(String message) {
        if (Village.saveToFile) {
            eventMessageToFileSaver.save(message);
        }
    }

    public static int getCitizensQuantity() {
        return CITIZENS_QUANTITY;
    }

    public static List<Citizen> getCitizens() {
        return citizens;
    }

    public static List<Calendar> getMoonLightDaysArray() {
        return moonLightDays;
    }

    public static Calendar getEndLife() {
        return endLife;
    }
}
