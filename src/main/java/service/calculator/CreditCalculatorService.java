package service.calculator;

import dto.Person;

public class CreditCalculatorService {

    private final String NOT_EQUALS_NAME = "Bob";

    public boolean calculateCredit(Person person) {
        boolean creditResult = false;
        if (person.getAge() > 18 && !person.getName().equals(NOT_EQUALS_NAME)) {
            creditResult = true;
            person.setCreditEnable(true);
        }
        return creditResult;

    }
}
