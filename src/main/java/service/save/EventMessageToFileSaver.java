package service.save;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;

public class EventMessageToFileSaver {

    private PrintWriter printWriter;

    public void init() throws IOException {
        File file = new File("src/main/resources/fileVillage.txt");
        if (file.isFile()) {
            Files.newBufferedWriter(file.toPath(), StandardOpenOption.TRUNCATE_EXISTING);
        }
        FileWriter fileWriter = new FileWriter(file, true);
        printWriter = new PrintWriter(fileWriter);
    }

    public void save(String message) {
        printWriter.println(message);
        printWriter.flush();
    }
}
