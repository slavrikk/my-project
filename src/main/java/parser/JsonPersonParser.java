package parser;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import dto.Person;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.util.List;

public class JsonPersonParser {

    private final Gson gson = new Gson();

    public Person parse(String filePath) throws FileNotFoundException {
        Reader readerPerson = new FileReader(filePath);
        return gson.fromJson(readerPerson, Person.class);
    }

    public List<Person> parseList(String filePath) throws FileNotFoundException {
        Reader reader = new FileReader(filePath);

        return gson.fromJson(reader, new TypeToken<List<Person>>() {
        }.getType());
    }
}
