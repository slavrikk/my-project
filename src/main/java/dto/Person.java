package dto;
//data transfer object
public class Person {

    private int age;
    private int sum;
    private String name;
    private boolean creditEnable;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isCreditEnable() {
        return creditEnable;
    }

    public void setCreditEnable(boolean creditEnable) {
        this.creditEnable = creditEnable;
    }
}
