package village.calculator;

import com.google.gson.JsonObject;
import dto.Person;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import parser.JsonPersonParser;
import service.calculator.CreditCalculatorService;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class CreditCalculatorTest {

    static JsonPersonParser jsonPersonParser = new JsonPersonParser();
    CreditCalculatorService creditCalculatorService = new CreditCalculatorService();
    static List<Person> personList = new ArrayList<>();

    @BeforeAll
    static void parseFile() throws FileNotFoundException {
        personList = jsonPersonParser.parseList("src/main/resources/test.json");
    }

    @Test
    void creditCalculatorFalseTest() {
        Person danPerson = personList.stream()
                .filter(person -> person.getName().equals("Dan"))
                .findFirst()
                .orElseThrow();
        Assertions.assertFalse(creditCalculatorService.calculateCredit(danPerson));
    }

    @Test
    void creditCalculatorTrueTest() {
        Person binPerson = personList.stream()
                .filter(person -> person.getName().equals("Bin"))
                .findFirst()
                .orElseThrow();
        Assertions.assertTrue(creditCalculatorService.calculateCredit(binPerson));
    }
}
