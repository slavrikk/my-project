package village.unit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import village.Citizen;
import village.Race;
import village.Village;

import java.util.Calendar;
import java.util.List;

public class VillageTest {

    @Test
    void generateCitizensTest() {
        Village.generateCitizens();
        Assertions.assertEquals(Village.getCitizens().size(), Village.getCitizensQuantity());
    }

    @Test
    void generateRaceTest() {
        Citizen citizen = Village.getRace(1);
        Assertions.assertEquals(citizen.getRace(), Race.Vampire);
    }

    @Test
    void moonLightDaysLessThanEnaLifeDateTest() {
        Village.getMoonLightDays();
        List<Calendar> moonLightDaysArray = Village.getMoonLightDaysArray();
        moonLightDaysArray.forEach(calendar -> Assertions.assertTrue(calendar.before(Village.getEndLife())));
    }

}
