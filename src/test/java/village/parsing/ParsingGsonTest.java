package village.parsing;

import dto.Person;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import parser.JsonPersonParser;

import java.io.FileNotFoundException;
import java.util.List;

public class ParsingGsonTest {

    JsonPersonParser jsonPersonParser = new JsonPersonParser();

    @Test
    void parsingTestWithOnePerson() throws FileNotFoundException {

        Person person = jsonPersonParser.parse("src/main/resources/test_one_person.json");

        Assertions.assertEquals(person.getAge(), 20);
        Assertions.assertEquals(person.getSum(), 1000);
        Assertions.assertEquals(person.getName(), "Bob");


    }

    @Test
    void parsingTestWithMultiPersons() throws FileNotFoundException {
        List<Person> personList = jsonPersonParser.parseList("src/main/resources/test.json");

        Assertions.assertEquals(personList.size(), 4);
        personList.forEach(person -> Assertions.assertTrue(person.getAge() < 51));
    }

}
