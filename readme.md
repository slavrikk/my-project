Stack:
 Gradle 
 Spring Boot 
 Spring Data 
 Hibernate 
 Docker 
 Swagger 
 Allure 
 Rest Assured 
 Selenium

Подключение к Postgre через бинарник:
* скачать бинарник https://www.enterprisedb.com/download-postgresql-binaries
* внутри `\pgsql\bin`в консоле прописать `initdb -U root -D "PATH\pgsql\data"`, где `PATH` абсолютный путь до `pgsql`
  (пример: `initdb -U root -D "C:\Users\VShapkin\Desktop\pgsql\data"`)
* стартануть Postgre командой `pg_ctl -D "PATH\pgsql\data" start`, где `PATH` абсолютный путь до `pgsql`
    (пример: `pg_ctl -D "C:\Users\VShapkin\Desktop\pgsql\data" start`)
  
Остановить сервер `pg_ctl -D "PATH\pgsql\data" stop`.
Рестартонуть `pg_ctl -D "PATH\pgsql\data" restart`
    
    
